import { Present } from 'lingo-api';

import * as helper2 from './helper2';
import { GlobalSettings } from './helper1';

export default async function () {
	const something = helper2.makeSomething();
	console.log(something);

	const allSettings = await GlobalSettings.getAll();

	var a = 1;
	let tmp = 'Hello';

	for (var b = 0; b < a; b++) {
		Present(tmp);
	}
}