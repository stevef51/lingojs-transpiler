import { Facility } from 'lingo-api';

import * as moment from 'moment';
import { makeSomething } from './helper2';

const something = makeSomething();

console.log(`Hello Lingo World I have made ${something.something}`);
console.log(`The current time is ${moment().format()}`);

const GlobalSettings = Facility.globalSettings();

export {
	GlobalSettings
}