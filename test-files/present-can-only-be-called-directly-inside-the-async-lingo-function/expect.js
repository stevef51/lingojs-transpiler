const { expect } = require('chai');

const expectedError = "Present() can only be called directly inside the async Lingo function";

module.exports = (result) => {
	expect(result.Checklist.modules[0].diagnostics, `Expected error "${expectedError}"`).to.have.lengthOf(1);
	expect(result.Checklist.modules[0].diagnostics[0].messageText).to.equal(expectedError);
}