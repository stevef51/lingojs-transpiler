import { Present, IWorkingDocumentContext } from 'lingo-api';
import * as helper2 from './helper2';
import { GlobalSettings } from './helper1';

export default async function (context: IWorkingDocumentContext) {
	const something = helper2.makeSomething();
	console.log(something);

	console.log(context.checklistId);

	const allSettings = await GlobalSettings.getAll();

	var a = 1;
	let tmp: string = 'Hello';

	for (var b = 0; b < a; b++) {
		Present(tmp);
	}
}