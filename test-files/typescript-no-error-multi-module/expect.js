const { expect } = require('chai');

module.exports = (result) => {
	expect(result.Checklist.modules).to.have.lengthOf(3);
	result.Checklist.modules.map(module => {
		expect(module.diagnostics, `${result.Name}.${module.name} should have 0 diagnostics`).to.have.lengthOf(0);
		const jsOutput = module.outputs.find(o => o.type === 'js');
		expect(jsOutput).to.not.be.undefined;
	})
}