import { makeSomething } from './helper-not-found';
import * as moment from 'moment-not-found';

const something = makeSomethingBad();

console.log(`Hello Lingo World I have made ${something.somethingElse}`);
console.log(`The current time is ${moment().format()}`);