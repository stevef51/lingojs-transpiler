const { expect } = require('chai');

module.exports = (result) => {
	expect(result.Checklist.modules).to.have.lengthOf(3);
	const i_have_invalid_imports = result.Checklist.modules.find(m => m.name === 'i-have-invalid-imports');
	expect(i_have_invalid_imports).to.not.be.null;
	expect(i_have_invalid_imports.diagnostics).to.have.lengthOf(3);
}