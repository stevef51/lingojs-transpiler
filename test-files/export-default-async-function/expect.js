const { expect } = require('chai');

module.exports = (result) => {
	expect(result.Checklist.modules[0].diagnostics).to.have.lengthOf(0);
}