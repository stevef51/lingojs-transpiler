const { expect } = require('chai');

const expectedError = "'await Present()' is not allowed, remove the 'await'";
module.exports = (result) => {
	expect(result.Checklist.modules[0].diagnostics, `Expected error "${expectedError}"`).to.have.lengthOf(1);
	expect(result.Checklist.modules[0].diagnostics[0].messageText).to.equal(expectedError);
}