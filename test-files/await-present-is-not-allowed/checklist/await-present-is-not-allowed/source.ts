import { Present } from 'lingo-api';

export default async () => {
	await Present(`this should throw error "await Present()' is not allowed, remove the 'await'"`);
}