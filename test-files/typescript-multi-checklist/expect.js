const { expect } = require('chai');

module.exports = (result) => {
	expect(result.Checklist.modules).to.have.lengthOf(1);
	result.Checklist.modules.map(module => {
		expect(module.diagnostics).to.have.lengthOf(0);
		const jsOutput = module.outputs.find(o => o.type === 'js');
		expect(jsOutput).to.not.be.undefined;
	})
}