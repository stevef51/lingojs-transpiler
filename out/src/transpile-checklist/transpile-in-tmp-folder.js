"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.transpileChecklist = void 0;
const ts = require("typescript");
const types_1 = require("./types");
const process_1 = require("process");
const tmp = require("tmp");
const _fs = require("fs");
const Path = require("path");
const helpers_1 = require("./helpers");
const Bluebird = require("bluebird");
const _ = require("lodash");
const lingojs_plugin = require("../babel-plugin-lingo-js-present");
const babel = require("@babel/core");
const calculate_checklists_to_compile_1 = require("./calculate-checklists-to-compile");
const calculate_checklists_to_compile_2 = require("./calculate-checklists-to-compile");
const fs = Bluebird.promisifyAll(_fs);
const cwd = process_1.cwd();
function moduleName(folder, checklist, module) {
    return Path.join(folder, types_1.ChecklistFolder, checklist.Name, `${module.name}.${module.source.type}`);
}
async function layoutChecklistAndModule(checklistAndModule, folder) {
    const checklist = checklistAndModule.checklist;
    await fs.mkdirAsync(Path.join(folder, types_1.ChecklistFolder, checklist.Name), {
        recursive: true
    });
    const module = checklistAndModule.module;
    module.diagnostics = [];
    module.outputs = [];
    const source = {
        fileName: Path.join(folder, calculate_checklists_to_compile_2.getModulePath(checklistAndModule, true)),
        module
    };
    await fs.writeFileAsync(source.fileName, module.source.content);
    return source;
}
function simplifyDiagnostic(rootFolder, diagnostic) {
    const { file, relatedInformation } = diagnostic, copyOver = __rest(diagnostic, ["file", "relatedInformation"]);
    return Object.assign({ file: file ? Path.relative(rootFolder, file.fileName) : undefined, relatedInformation: relatedInformation === null || relatedInformation === void 0 ? void 0 : relatedInformation.map(ri => simplifyDiagnostic(rootFolder, ri)) }, copyOver);
}
function transpileChecklist(checklist, others) {
    // Create a temporary folder where we can layout a normal typescript/javascript project to be transpiled by Typescript compiler ..
    return new Promise((resolve, reject) => {
        tmp.dir({
            tmpdir: cwd,
            unsafeCleanup: true
        }, async (err, tmpFolder, cleanup) => {
            try {
                const checklistsToCompile = calculate_checklists_to_compile_1.default(checklist, others);
                const sources = _.flatten(await Promise.all((checklistsToCompile || []).map(c => layoutChecklistAndModule(c, tmpFolder))));
                const options = {
                    allowJs: true,
                    checkJs: true,
                    module: ts.ModuleKind.ESNext,
                    target: ts.ScriptTarget.ES2017,
                    noEmitOnError: true,
                    sourceMap: true,
                    declaration: true,
                    alwaysStrict: true,
                    moduleResolution: ts.ModuleResolutionKind.NodeJs,
                    outDir: Path.join(tmpFolder, 'out'),
                    baseUrl: tmpFolder,
                    //					types: [`lingojs-api`],							// Auto import these types
                    paths: {}
                };
                options.paths[`${types_1.ChecklistFolder}/*`] = [
                    `./${types_1.ChecklistFolder}/*`
                ];
                const defaultHost = ts.createCompilerHost(options);
                const comparePaths = (a, b) => {
                    return a.replace(/\\/g, '/') === b.replace(/\\/g, '/');
                };
                const overrideHost = {
                    writeFile(fileName, data, writeByteOrderMark, onError, sourceFiles) {
                        const source = sources.find(s => comparePaths(sourceFiles[0].fileName, s.fileName));
                        if (source != null) {
                            source.module.outputs = source.module.outputs || [];
                            source.module.outputs.push({
                                content: data,
                                type: helpers_1.typeFromExtension(fileName)
                            });
                        }
                    },
                    getCurrentDirectory() { return tmpFolder; }
                };
                const host = Object.assign(Object.assign({}, defaultHost), overrideHost);
                const program = ts.createProgram(sources.map(s => s.fileName), options, host);
                //			const diagnostics = ts.getPreEmitDiagnostics(program);
                const emitResult = program.emit();
                if (emitResult.diagnostics.length > 0) {
                    emitResult.diagnostics.map(d => {
                        const source = sources.find(s => { var _a, _b; return comparePaths(s.fileName, (_b = (_a = d.file) === null || _a === void 0 ? void 0 : _a.fileName) !== null && _b !== void 0 ? _b : ''); });
                        if (source != null) {
                            source.module.diagnostics = source.module.diagnostics || [];
                            source.module.diagnostics.push(simplifyDiagnostic(tmpFolder, d));
                        }
                        else {
                            checklist.Checklist.diagnostics = checklist.Checklist.diagnostics || [];
                            checklist.Checklist.diagnostics.push(simplifyDiagnostic(tmpFolder, d));
                        }
                    });
                }
                else {
                    const lingoSource = checklist.Checklist.modules.find(m => m.moduleType === 'lingo-js-source');
                    if (lingoSource != null) {
                        const jsSrc = lingoSource.outputs.find(o => o.type === 'js');
                        if (jsSrc != null) {
                            try {
                                const babelResult = await babel.transformAsync(jsSrc.content, {
                                    plugins: [
                                        lingojs_plugin,
                                        '@babel/plugin-transform-modules-commonjs' // This converts any "import" or "export" statements into CommonJs format
                                    ],
                                    sourceType: "module"
                                });
                                lingoSource.outputs.push({
                                    content: babelResult.code,
                                    type: 'js',
                                    tag: 'ES2017'
                                });
                                // Remove the intermediate Js file
                                jsSrc.type = 'txt';
                                //lingoSource.outputs = lingoSource.outputs.filter(h => h !== jsSrc);
                            }
                            catch (ex) {
                                lingoSource.diagnostics = lingoSource.diagnostics || [];
                                lingoSource.diagnostics.push({
                                    code: ex.code,
                                    category: types_1.DiagnosticCategory.Error,
                                    messageText: ex.message.replace(/^unknown:\s/, ''),
                                    start: ex.pos,
                                });
                            }
                        }
                    }
                    const helpers = checklist.Checklist.modules.filter(m => m.moduleType === 'lingo-js-helper'); // a helper module, still need to transpile "import/export" statements into CommonJs format
                    await Promise.all(helpers.map(async (helper) => {
                        const jsSrc = helper.outputs.find(o => o.type === 'js');
                        if (jsSrc != null) {
                            try {
                                const babelResult = await babel.transformAsync(jsSrc.content, {
                                    plugins: [
                                        '@babel/plugin-transform-modules-commonjs' // This converts any "import" or "export" statements into CommonJs format
                                    ],
                                    sourceType: "module"
                                });
                                helper.outputs.push({
                                    content: babelResult.code,
                                    type: 'js',
                                    tag: 'ES2017'
                                });
                                // Remove the intermediate Js file
                                jsSrc.type = 'txt';
                                helper.outputs = helper.outputs.filter(h => h !== jsSrc);
                            }
                            catch (ex) {
                                helper.diagnostics = helper.diagnostics || [];
                                helper.diagnostics.push({
                                    code: ex.code,
                                    category: types_1.DiagnosticCategory.Error,
                                    messageText: ex.message.replace(/^unknown:\s/, ''),
                                    start: ex.pos,
                                });
                            }
                        }
                    }));
                }
                resolve(checklist);
            }
            catch (ex) {
                reject(ex);
            }
            finally {
                cleanup();
            }
        });
    });
}
exports.transpileChecklist = transpileChecklist;
//# sourceMappingURL=transpile-in-tmp-folder.js.map