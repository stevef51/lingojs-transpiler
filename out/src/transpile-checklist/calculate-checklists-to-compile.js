"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getModulePath = void 0;
const types_1 = require("./types");
const ts = require("typescript");
const Path = require("path");
function getModulePath(arg, withExtension = false) {
    return Path.join(types_1.ChecklistFolder, arg.checklist.Name, withExtension ? `${arg.module.name}.${arg.module.source.type}` : arg.module.name);
}
exports.getModulePath = getModulePath;
function calculateChecklistsToCompile(checklist, others) {
    let othersImported = new Map();
    function isAlreadyImported(checklistAndModule) {
        const key = getModulePath(checklistAndModule);
        const result = othersImported.has(key);
        if (!result) {
            othersImported.set(key, checklistAndModule);
        }
        return result;
    }
    const all = others.concat(checklist);
    function resolveModule(name, from, others) {
        let resolvingPath = name;
        if (resolvingPath.startsWith('.' + Path.sep) || resolvingPath.startsWith('..' + Path.sep)) {
            // relative, Path.resolve will work
        }
        else if (resolvingPath.startsWith(Path.sep)) {
            // absolute, Path.resolve will work (although not sure which modules should be imported from absolute path)
        }
        else {
            // likely either 'checklist/...' or a node_module 
            if (resolvingPath.startsWith(types_1.ChecklistFolder)) {
                resolvingPath = Path.sep + resolvingPath;
            }
            else {
                // Likely a node_module import
                return;
            }
        }
        // We should get here with either
        // 1. Relative path eg './helper1'
        // 2. Absolute path eg '/checklist/common stuff/helper1'
        const fullPath = Path.resolve(Path.sep + Path.join(types_1.ChecklistFolder, from.checklist.Name), resolvingPath);
        const split = fullPath.split(Path.sep);
        if (split[1] === types_1.ChecklistFolder) {
            const otherChecklist = all.find(o => o.Name === split[2]); // We know split[1] === 'checklist'
            const otherModule = otherChecklist ? otherChecklist.Checklist.modules.find(m => m.name === split[3]) : null;
            if (otherModule != null) {
                const otherChecklistAndModule = { checklist: otherChecklist, module: otherModule };
                findImportedModules(otherChecklistAndModule);
            }
        }
    }
    function findImportedModules(checklistAndModule) {
        if (isAlreadyImported(checklistAndModule)) {
            return;
        }
        const sourceFile = ts.createSourceFile(getModulePath(checklistAndModule), checklistAndModule.module.source.content, ts.ScriptTarget.ES2017, true, checklistAndModule.module.source.type === 'js' ? ts.ScriptKind.JS : ts.ScriptKind.TS);
        sourceFile.forEachChild(node => {
            var _a;
            if (ts.isImportDeclaration(node)) {
                const moduleName = ((_a = node.moduleSpecifier.text) !== null && _a !== void 0 ? _a : '');
                resolveModule(moduleName, checklistAndModule, others);
            }
        });
    }
    function findImportedChecklists(checklist) {
        // So each checklist may use external dependencies, we are going to work out what these are ..
        checklist.Checklist.modules.map(module => {
            const checklistAndModule = { checklist, module };
            if (module.moduleType === 'lingo-js-helper' || module.moduleType === 'lingo-js-source') {
                findImportedModules(checklistAndModule);
            }
        });
    }
    findImportedChecklists(checklist);
    let result = [];
    othersImported.forEach(imported => {
        result.push(imported);
    });
    return result;
}
exports.default = calculateChecklistsToCompile;
//# sourceMappingURL=calculate-checklists-to-compile.js.map