"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
function calculateDependencies(checklist) {
    // So each checklist may use external dependencies, we are going to work out what these are ..
    let deps = [];
    checklist.Checklist.modules.map(module => {
        if (module.moduleType === 'lingo-js-helper' || module.moduleType === 'lingo-js-source') {
            const sourceFile = ts.createSourceFile(`/checklist/${checklist.Name}/${module.name}.${module.source.type}`, module.source.content, ts.ScriptTarget.ES2017, true, module.source.type === 'js' ? ts.ScriptKind.JS : ts.ScriptKind.TS);
            sourceFile.forEachChild(node => {
                if (ts.isImportDeclaration(node)) {
                    if (ts.isNamedImports(node.importClause.namedBindings)) {
                        node.importClause.namedBindings.elements.map(e => {
                            deps.push(e.name.escapedText);
                        });
                    }
                }
            });
        }
    });
    return deps;
}
exports.default = calculateDependencies;
//# sourceMappingURL=calculate-dependecies.js.map