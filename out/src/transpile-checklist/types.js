"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiagnosticCategory = exports.ChecklistFolder = void 0;
exports.ChecklistFolder = 'checklist';
/* Diagnostics are extracted from the Typescript library and simplified */
var DiagnosticCategory;
(function (DiagnosticCategory) {
    DiagnosticCategory[DiagnosticCategory["Warning"] = 0] = "Warning";
    DiagnosticCategory[DiagnosticCategory["Error"] = 1] = "Error";
    DiagnosticCategory[DiagnosticCategory["Suggestion"] = 2] = "Suggestion";
    DiagnosticCategory[DiagnosticCategory["Message"] = 3] = "Message";
})(DiagnosticCategory = exports.DiagnosticCategory || (exports.DiagnosticCategory = {}));
//# sourceMappingURL=types.js.map