"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeFromExtension = void 0;
function typeFromExtension(filename) {
    if (filename.endsWith('.d.ts')) {
        return "d.ts";
    }
    else if (filename.endsWith('.js.map')) {
        return "js.map";
    }
    else if (filename.endsWith('.ts.map')) {
        return "ts.map";
    }
    else if (filename.endsWith('.js')) {
        return "js";
    }
    else if (filename.endsWith('.ts')) {
        return "ts";
    }
    return null;
}
exports.typeFromExtension = typeFromExtension;
//# sourceMappingURL=helpers.js.map