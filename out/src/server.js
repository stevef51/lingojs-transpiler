"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
require('express-async-errors');
const port = process.env.port || 80;
const virtualDirPath = process.env.virtualDirPath || '';
console.log(`Virtual path is ${virtualDirPath}`);
const app = express();
const transpile_checklist_1 = require("./transpile-checklist");
const api = express.Router();
api.post(`/transpile-checklist`, express.json(), async (req, res) => {
    const request = req.body;
    const result = await transpile_checklist_1.transpileChecklist(request.checklist, request.otherChecklists);
    res.send(result);
});
app.use(`${virtualDirPath}/api`, api);
app.use((err, req, res, next) => {
    if (err.status) {
        res.status(err.status);
        res.json({
            error: err.message
        });
    }
    else {
        res.status(503);
        res.json({
            error: err.message
        });
    }
    next(err);
});
app.listen(port, () => {
    console.log(`Listening on ${port}`);
});
//# sourceMappingURL=server.js.map