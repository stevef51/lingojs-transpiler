"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transpile_checklist_1 = require("./src/transpile-checklist");
const fs = require("fs");
const Path = require("path");
const rimraf = require("rimraf");
function makeTestChecklist(folder) {
    const files = fs.readdirSync(folder);
    return {
        Name: Path.basename(folder),
        Checklist: {
            modules: files.map(file => {
                const name = Path.basename(file, Path.extname(file));
                return {
                    name,
                    moduleType: name === 'source' ? 'lingo-js-source' : 'lingo-js-helper',
                    source: {
                        type: Path.extname(file).substr(1),
                        content: fs.readFileSync(Path.join(folder, file), { encoding: 'utf-8' }),
                    },
                    outputs: []
                };
            })
        }
    };
}
function makeTestChecklistFromFolder(testName) {
    const testNameFolder = Path.join('test-files', testName, 'checklist');
    const otherDirs = fs.readdirSync(testNameFolder);
    return {
        test: makeTestChecklist(Path.join(testNameFolder, testName)),
        others: otherDirs.filter(d => d !== testName).map(d => makeTestChecklist(Path.join(testNameFolder, d)))
    };
}
async function testChecklist(folder, writeToDisk = false) {
    console.log(`Testing ${folder}...`);
    const { test, others } = makeTestChecklistFromFolder(folder);
    const result = await transpile_checklist_1.transpileChecklist(test, others);
    if (writeToDisk) {
        const outFolder = Path.join('test-output', folder);
        await new Promise((resolve, reject) => {
            rimraf(outFolder, (err) => {
                fs.mkdir(outFolder, {
                    recursive: true
                }, (err, path) => {
                    result.Checklist.modules.map(m => {
                        m.outputs.map(o => {
                            const outFile = Path.join(outFolder, `${o.tag || ''}${m.name}.${o.type}`);
                            fs.writeFileSync(outFile, o.content);
                        });
                        if (m.diagnostics.length > 0) {
                            const diagFile = Path.join(outFolder, 'diagnostics.txt');
                            fs.writeFileSync(diagFile, m.diagnostics.map(d => d.messageText).join('\n'));
                        }
                    });
                    resolve();
                });
            });
        });
    }
    let expect = null;
    try {
        expect = require(`../test-files/${folder}/expect`); // This is run relative to ./out
    }
    catch (_a) {
        // No such expect.js
    }
    if (expect != null) {
        expect(result);
    }
}
async function testChecklists(writeToDisk = false, only) {
    const tests = fs.readdirSync('test-files');
    for (const test of tests) {
        if (only == null || only.indexOf(test) >= 0) {
            await testChecklist(test, writeToDisk);
        }
    }
}
testChecklists(true);
//testChecklist('top-level-await', true);
//# sourceMappingURL=test.js.map