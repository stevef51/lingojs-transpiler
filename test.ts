import * as chai from 'chai';
import { transpileChecklist } from './src/transpile-checklist';
import * as fs from 'fs';
import * as Path from 'path';
import { OfflineChecklistModel, ChecklistModuleContentType } from './src/transpile-checklist/types';
import * as rimraf from 'rimraf';

function makeTestChecklist(folder): OfflineChecklistModel {
	const files = fs.readdirSync(folder);

	return {
		Name: Path.basename(folder),
		Checklist: {
			modules: files.map(file => {
				const name = Path.basename(file, Path.extname(file))
				return {
					name,
					moduleType: name === 'source' ? 'lingo-js-source' : 'lingo-js-helper',
					source: {
						type: Path.extname(file).substr(1) as ChecklistModuleContentType,
						content: fs.readFileSync(Path.join(folder, file), { encoding: 'utf-8' }),
					},
					outputs: []
				}
			})
		}
	}
}

function makeTestChecklistFromFolder(testName: string) {
	const testNameFolder = Path.join('test-files', testName, 'checklist');
	const otherDirs = fs.readdirSync(testNameFolder);
	return {
		test: makeTestChecklist(Path.join(testNameFolder, testName)),
		others: otherDirs.filter(d => d !== testName).map(d => makeTestChecklist(Path.join(testNameFolder, d)))
	}
}

async function testChecklist(folder: string, writeToDisk: boolean = false) {
	console.log(`Testing ${folder}...`);
	const { test, others } = makeTestChecklistFromFolder(folder);

	const result = await transpileChecklist(test, others);

	if (writeToDisk) {
		const outFolder = Path.join('test-output', folder);
		await new Promise((resolve, reject) => {
			rimraf(outFolder, (err) => {
				fs.mkdir(outFolder, {
					recursive: true
				}, (err, path) => {
					result.Checklist.modules.map(m => {
						m.outputs.map(o => {
							const outFile = Path.join(outFolder, `${o.tag || ''}${m.name}.${o.type}`);
							fs.writeFileSync(outFile, o.content);
						});
						if (m.diagnostics.length > 0) {
							const diagFile = Path.join(outFolder, 'diagnostics.txt');
							fs.writeFileSync(diagFile, m.diagnostics.map(d => d.messageText).join('\n'));
						}
					})
					resolve();
				})
			});
		});
	}

	let expect: (result: OfflineChecklistModel) => void | null = null;
	try {
		expect = require(`../test-files/${folder}/expect`);			// This is run relative to ./out
	} catch {
		// No such expect.js
	}
	if (expect != null) {
		expect(result);
	}

}

async function testChecklists(writeToDisk: boolean = false, only?: string[]) {
	const tests = fs.readdirSync('test-files');
	for (const test of tests) {
		if (only == null || only.indexOf(test) >= 0) {
			await testChecklist(test, writeToDisk);
		}
	}
}

testChecklists(true);
//testChecklist('top-level-await', true);
