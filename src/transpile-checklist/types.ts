export const ChecklistFolder = 'checklist';

export type ChecklistModuleType = 'resource' | 'lingo-js-source' | 'lingo-js-helper' | 'page-scope-controller';
export type ChecklistModuleContentType = 'css' | 'html' | 'txt' | 'json' | 'js' | 'ts' | 'd.ts' | 'js.map' | 'ts.map';

export interface ChecklistModuleContent {
	type: ChecklistModuleContentType;
	content: string;
	tag?: string;				// Outputs can be grouped together with the same tag
}

/* Diagnostics are extracted from the Typescript library and simplified */
export enum DiagnosticCategory {
	Warning = 0,
	Error = 1,
	Suggestion = 2,
	Message = 3
}

export interface DiagnosticMessageChain {
	messageText: string;
	category: DiagnosticCategory;
	code: number;
	next?: DiagnosticMessageChain[];
}

export interface Diagnostic {
	category: DiagnosticCategory;
	code: number;
	file?: string;
	start?: number | undefined;
	length?: number | undefined;
	messageText: string | DiagnosticMessageChain;
	source?: string;
	relatedInformation?: Diagnostic[];
}

export interface ChecklistModule {
	name: string;
	moduleType: ChecklistModuleType;
	source: ChecklistModuleContent;
	outputs: ChecklistModuleContent[];
	diagnostics?: Diagnostic[];
}

export interface OfflineChecklistModel {
	Name: string;
	Checklist: {
		modules: ChecklistModule[];
		diagnostics?: Diagnostic[];
	}
}

