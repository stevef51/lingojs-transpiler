
import * as ts from 'typescript';
import { ChecklistModule, OfflineChecklistModel, ChecklistModuleContentType, Diagnostic as ChecklistDiagnostic, DiagnosticCategory, ChecklistFolder } from './types';
import { cwd as processCwd } from 'process';
import * as tmp from 'tmp';
import * as _fs from 'fs';
import * as Path from 'path';
import { typeFromExtension } from './helpers';
import * as Bluebird from 'bluebird';
import * as _ from 'lodash';
import * as lingojs_plugin from '../babel-plugin-lingo-js-present';
import * as babel from '@babel/core';
import calculateChecklistsToCompile from './calculate-checklists-to-compile';
import { ChecklistAndModule, getModulePath } from './calculate-checklists-to-compile';

const fs = Bluebird.promisifyAll(_fs) as any;

const cwd = processCwd();

function moduleName(folder: string, checklist: OfflineChecklistModel, module: ChecklistModule) {
	return Path.join(folder, ChecklistFolder, checklist.Name, `${module.name}.${module.source.type}`)
}

interface Source {
	fileName: string;
	module: ChecklistModule;
}

async function layoutChecklistAndModule(checklistAndModule: ChecklistAndModule, folder: string): Promise<Source> {
	const checklist = checklistAndModule.checklist;
	await fs.mkdirAsync(Path.join(folder, ChecklistFolder, checklist.Name), {
		recursive: true
	});

	const module = checklistAndModule.module;
	module.diagnostics = [];
	module.outputs = [];
	const source = {
		fileName: Path.join(folder, getModulePath(checklistAndModule, true)),
		module
	}
	await fs.writeFileAsync(source.fileName, module.source.content);
	return source;
}

function simplifyDiagnostic(rootFolder: string, diagnostic: ts.Diagnostic): ChecklistDiagnostic {
	const { file, relatedInformation, ...copyOver } = diagnostic;
	return {
		file: file ? Path.relative(rootFolder, file.fileName) : undefined,
		relatedInformation: relatedInformation?.map(ri => simplifyDiagnostic(rootFolder, ri)),
		...copyOver
	}
}

export function transpileChecklist(checklist: OfflineChecklistModel, others: OfflineChecklistModel[]): Promise<OfflineChecklistModel> {
	// Create a temporary folder where we can layout a normal typescript/javascript project to be transpiled by Typescript compiler ..
	return new Promise((resolve, reject) => {
		tmp.dir({
			tmpdir: cwd,
			unsafeCleanup: true
		}, async (err, tmpFolder, cleanup) => {
			try {

				const checklistsToCompile = calculateChecklistsToCompile(checklist, others);
				const sources = _.flatten(await Promise.all((checklistsToCompile || []).map(c => layoutChecklistAndModule(c, tmpFolder))));

				const options: ts.CompilerOptions = {
					allowJs: true,
					checkJs: true,

					module: ts.ModuleKind.ESNext,
					target: ts.ScriptTarget.ES2017,
					noEmitOnError: true,
					sourceMap: true,
					declaration: true,
					alwaysStrict: true,
					moduleResolution: ts.ModuleResolutionKind.NodeJs,
					outDir: Path.join(tmpFolder, 'out'),			// Not really needed since we dont write to disk but 'tsc' will check for .js files before faulting on overwriting them
					baseUrl: tmpFolder,
					paths: {
					}
				}
				options.paths[`${ChecklistFolder}/*`] = [
					`./${ChecklistFolder}/*`
				];
				const defaultHost = ts.createCompilerHost(options);
				const comparePaths = (a: string, b: string) => {
					return a.replace(/\\/g, '/') === b.replace(/\\/g, '/');
				}
				const overrideHost: Partial<ts.CompilerHost> = {
					writeFile(fileName: string, data: string, writeByteOrderMark: boolean, onError?: (message: string) => void, sourceFiles?: readonly ts.SourceFile[]) {
						const source = sources.find(s => comparePaths(sourceFiles[0].fileName, s.fileName));
						if (source != null) {
							source.module.outputs = source.module.outputs || [];
							source.module.outputs.push({
								content: data,
								type: typeFromExtension(fileName)
							})
						}
					},
					getCurrentDirectory() { return tmpFolder }
				}
				const host: ts.CompilerHost = { ...defaultHost, ...overrideHost };
				const program = ts.createProgram(sources.map(s => s.fileName), options, host);
				//			const diagnostics = ts.getPreEmitDiagnostics(program);
				const emitResult = program.emit();
				if (emitResult.diagnostics.length > 0) {
					emitResult.diagnostics.map(d => {
						const source = sources.find(s => comparePaths(s.fileName, d.file?.fileName ?? ''));
						if (source != null) {
							source.module.diagnostics = source.module.diagnostics || [];
							source.module.diagnostics.push(simplifyDiagnostic(tmpFolder, d));
						} else {
							checklist.Checklist.diagnostics = checklist.Checklist.diagnostics || [];
							checklist.Checklist.diagnostics.push(simplifyDiagnostic(tmpFolder, d));
						}
					})
				} else {
					const lingoSource = checklist.Checklist.modules.find(m => m.moduleType === 'lingo-js-source');
					if (lingoSource != null) {
						const jsSrc = lingoSource.outputs.find(o => o.type === 'js');
						if (jsSrc != null) {
							try {
								const babelResult = await babel.transformAsync(jsSrc.content, {
									plugins: [
										lingojs_plugin,		// This does our Lingo "Present" work
										'@babel/plugin-transform-modules-commonjs'	// This converts any "import" or "export" statements into CommonJs format
									],
									sourceType: "module"
								})
								lingoSource.outputs.push({
									content: babelResult.code,
									type: 'js',
									tag: 'ES2017'
								})

								// Remove the intermediate Js file
								jsSrc.type = 'txt';
								//lingoSource.outputs = lingoSource.outputs.filter(h => h !== jsSrc);
							} catch (ex) {
								lingoSource.diagnostics = lingoSource.diagnostics || [];
								lingoSource.diagnostics.push({
									code: ex.code,
									category: DiagnosticCategory.Error,
									messageText: ex.message.replace(/^unknown:\s/, ''),
									start: ex.pos,
								})
							}
						}
					}
					const helpers = checklist.Checklist.modules.filter(m => m.moduleType === 'lingo-js-helper');	// a helper module, still need to transpile "import/export" statements into CommonJs format
					await Promise.all(helpers.map(async helper => {
						const jsSrc = helper.outputs.find(o => o.type === 'js');
						if (jsSrc != null) {
							try {
								const babelResult = await babel.transformAsync(jsSrc.content, {
									plugins: [
										'@babel/plugin-transform-modules-commonjs'	// This converts any "import" or "export" statements into CommonJs format
									],
									sourceType: "module"
								})
								helper.outputs.push({
									content: babelResult.code,
									type: 'js',
									tag: 'ES2017'
								})

								// Remove the intermediate Js file
								jsSrc.type = 'txt';
								helper.outputs = helper.outputs.filter(h => h !== jsSrc);
							} catch (ex) {
								helper.diagnostics = helper.diagnostics || [];
								helper.diagnostics.push({
									code: ex.code,
									category: DiagnosticCategory.Error,
									messageText: ex.message.replace(/^unknown:\s/, ''),
									start: ex.pos,
								})
							}
						}
					}));
				}

				resolve(checklist);
			} catch (ex) {
				reject(ex);
			} finally {
				cleanup();
			}
		})
	});
}
