import { ChecklistModuleContentType } from "./types";

export function typeFromExtension(filename: string): ChecklistModuleContentType | null {
	if (filename.endsWith('.d.ts')) {
		return "d.ts";
	} else if (filename.endsWith('.js.map')) {
		return "js.map";
	} else if (filename.endsWith('.ts.map')) {
		return "ts.map";
	} else if (filename.endsWith('.js')) {
		return "js";
	} else if (filename.endsWith('.ts')) {
		return "ts";
	}
	return null;
}

