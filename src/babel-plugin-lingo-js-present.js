/* 
This module exports a single function which takes a LingoJs source code string (which is pure Javascript) and transpiles it to 
pure Javascript (outputting source code & source maps) but making any 'Present(something)' statements special to work in an client side LingoJs player

For more help on the BabelJs docs see

https://babeljs.io/docs/en/
https://github.com/jamiebuilds/babel-handbook/blob/master/translations/en/plugin-handbook.md#babel-traverse

*/

import regenerator_runtime_plugin from '@babel/plugin-transform-regenerator';

const _ = require('lodash');

export default function (babel) {
	const { types: t } = babel;

	/* We take the src which could include 
	
			await fn();
			doSomethingOnce();
			Present(something);
	
	wrap it in an async function to allow embedded await's
	
		async function Lingo() {
			await fn();
			doSomethingOnce();
			Present(something);
		}
	
	Stage1 & Stage2: and we make an intermediate AST of the following
	
		async function Lingo() {
			___await(fn());
			doSomethingOnce();
			await Present(something);
		});
	
	Stage3: we then pass this through Babel with the 'plugin-transform-regenerator' plugin which will do most of the magic for us
	in splitting out the (await'ed) Present() statements into a statemachined function, generating ..
	
		Lingo(() => {
			return regeneratorRuntime.async(function _callee$(_context) {
				while (1) switch (_context.prev = _context.next) {
				case 0:
					___await(fn());
	
					doSomethingOnce();
					_context.next = 3;
					return regeneratorRuntime.awrap(Present(something));
	
				case 3:
				case "end":
					return _context.stop();
				}
			});
		});
	
	Stage4: we then take this AST, replace bits to get
	
		async function Lingo(_context) {
			while (_context.running()) {
				switch (_context.prev = _context.next) {
					case 0:
						await fn();
	
						doSomethingOnce();
						_context.next = 3;
						_context.Present(something);
						break;
	
					case 3:
					case "end":
						return _context.stop();
					}
				}
			}
			return _context.presentArgs();
		});
	
	Stage 5, 6 & 7: However the 'Babel await magic' doesn't quite do exactly what we need since we need every 'Present()' to be
	in its own state machine step (read 'case:') so that we can 'previous' to it without re-running unwanted code before it
	so we insert a 'case ?:' for each Present(), we have to keep track of _context.next = ? references since we re-index the cases and update
	these references aswell to finally generate ..
	
		async function Lingo(_context) {
			while (_context.running()) {
				switch (_context.prev = _context.next) {
					case 0:
						await fn();
	
						doSomethingOnce();
						_context.next = 1;
						break;
	
					case 1:                           <-- Present has its own case, allows us to 'Previous' back to it without re-executing 'doSomethingOnce()' above
						_context.next = 2;
						_context.Present(something);
						break;
	
					case 2:
					case "end":
						return _context.stop();
					}
				}
			}
			return _context.presentArgs();
		});
	
	*/

	function throwError(path, msg) {
		const err = new SyntaxError(msg);
		err.pos = path.node.start;
		err.loc = path.node.loc;
		throw err;
	}

	// This visitor will replace a variable 'oldName' with a pre-built AST reference to 'this.locals.{oldName}'
	// This visitor was constructed based on the Babel 
	const localiseVariable = {
		ReferencedIdentifier(path, state) {
			if (path.node.name === state.oldName) {
				path.replaceWith(state.ref);
			}
		},

		/*    Scope(path, state) {
				if (!path.scope.bindingIdentifierEquals(state.oldName, state.binding.identifier)) {
					path.skip();
				}
			},
		*/
		AssignmentExpression(path, state) {
			const ids = path.getOuterBindingIdentifiers();

			for (const name in ids) {
				if (name === state.oldName) {
					if (state.allowAssign) {
						path.get('left').replaceWith(state.ref);
					} else {
						throwError(path, `Cannot assign to a const`);
					}
				}
			}
		}
	}

	const stage1 = {
		// Convert all original 'await fn()' calls into '___await(fn())', we will undo in stage3
		AwaitExpression: function (path) {
			if (t.isCallExpression(path.node.argument) && t.isIdentifier(path.node.argument.callee, { name: 'Present' })) {
				throwError(path, `'await Present()' is not allowed, remove the 'await'`);
			}
			path.replaceWith(t.callExpression(t.identifier('___await'), [path.node.argument]));
		},

		FunctionDeclaration: function (path) {
			if (t.isExportDefaultDeclaration(path.parent)) {
				// this is our "Lingo" function
				path.node.lingoFunction = true;
				this.lingoFunction = path.node;
			}
		},

		// All locally declared variables and references to them get moved to 'this.locals.?' so we can persist them over state machine calls
		VariableDeclaration: function (path) {
			// Variables declared with 'let' do not get special treatment, these should only be used between Present statements
			if (path.node.kind === 'let') {
				return;
			}
			// Only do this within the Lingo function ..
			let parentFunc = path.findParent(p => p.isFunction() || p.isProgram());
			if (!parentFunc.node.lingoFunction) {
				return;
			}
			let locals = [];
			let thisDot = path.node.kind === 'const' ? 'consts' : 'locals';
			path.node.declarations.forEach((decl, index) => {
				// create 'this.locals.?' expression
				let ref = t.memberExpression(t.memberExpression(this.contextId, t.identifier(thisDot)), decl.id);
				let declPath = path.get(`declarations.${index}`);

				// find all references including Assignments to our declared variable and replace with 'this.locals.?'
				declPath.scope.traverse(declPath.scope.block, localiseVariable, {
					oldName: decl.id.name,
					ref,
					allowAssign: path.node.kind !== 'const'
				})

				let declInit = decl.init || t.identifier('undefined');
				if (path.node.kind === 'const') {
					declInit = t.callExpression(t.identifier('Const'), [decl.init]);
				}
				// replace the initialisation aswell
				locals.push(t.expressionStatement(t.assignmentExpression('=', ref, declInit)));
			});

			// this replaces the 'var x, y = 1' statement with 'this.locals.x = undefined; this.locals.y = 1;'
			path.replaceWithMultiple(locals);
		}
	}

	const stage2 = {
		// Find all 'Present(something)' calls and replace with 'await Present(something)' in prep for BabelJs magic
		CallExpression: function (path) {
			if (path.node.callee.name === 'Present' && !path.node.lingoized) {
				path.node.lingoized = true;
				// Make sure the calling function is our async Lingo Function
				let parentFunc = path.findParent(p => p.isFunction() || p.isProgram());
				if (!parentFunc.node.lingoFunction) {
					throwError(path, `Present() can only be called directly inside the async Lingo function`);
				}
				path.get('callee').replaceWith(t.memberExpression(this.contextId, path.node.callee));
				let statementRoot = path.getStatementParent();
				let expressionPath = statementRoot.get('expression');
				expressionPath.replaceWith(t.awaitExpression(expressionPath.node));
			}
		}
	}

	// stage after BabelJs magic (stage3) ..
	const stage4 = {
		// replace all '___await(fn())' with 'await fn()' to restore original await intentions
		CallExpression: function (path) {
			if (t.isIdentifier(path.node.callee, { name: '___await' })) {
				path.replaceWith(t.awaitExpression(path.node.arguments[0]));
			}
		},

		ReturnStatement: function (path) {
			// replace all 'return regeneratorRuntime.awrap(this.Present(something));' with 'this.Present(something);'
			if (t.isCallExpression(path.node.argument)) {
				let awrapCall =
					t.isMemberExpression(path.node.argument.callee) &&
						t.isIdentifier(path.node.argument.callee.object, { name: 'regeneratorRuntime' }) &&
						t.isIdentifier(path.node.argument.callee.property, { name: 'awrap' })
						? path.node.argument : null;
				if (awrapCall) {
					let presentCall = awrapCall.arguments[0];
					path.replaceWith(presentCall);
					path.insertAfter(t.breakStatement());

					// Mark this so its easy to find later
					presentCall.presentCall = true;
				}
			}
			// there should be the following code
			//
			// return regeneratorRuntime.async(function _callee(_context) {
			//   while (1) switch(_context.prev = _context.next) {
			//     ...
			//   }
			// }, null, this);
			//
			// we replace the above with
			//
			// while(this.running()) {
			//   switch(this.prev = this.next) {
			//     ...
			//   }
			// }
			// return this.presentArgs();
			if (t.isCallExpression(path.node.argument)) {
				let call = path.node.argument;
				if (t.isMemberExpression(call.callee) && t.isIdentifier(call.callee.object, { name: 'regeneratorRuntime' }) && t.isIdentifier(call.callee.property, { name: 'async' })) {
					let fn = call.arguments.length && call.arguments[0];
					if (fn && t.isFunctionExpression(fn) && t.isIdentifier(fn.id) && fn.params.length) {
						let body = path.get('argument.arguments.0.body');

						// Can't simply call rename(fn.params[0].name, 'this') as that generates AST using Identifier('this') which is different to ThisExpression()
						// so we instead have to manually replace all references ..
						body.scope.bindings[fn.params[0].name].referencePaths.forEach(rp => {
							rp.replaceWith(this.contextId);
						})

						// Keep track of the switchClause ..
						this.switchClause = call.arguments[0].body.body[0].body;
						path.replaceWithMultiple([
							t.whileStatement(
								t.callExpression(
									t.memberExpression(this.contextId, t.identifier('running')),
									[]
								),
								t.blockStatement([this.switchClause])
							),
							t.returnStatement(t.callExpression(t.memberExpression(this.contextId, t.identifier('presentArgs')), []))
						]);
					}
				}
			}
		}
	}

	const stage5 = {
		// Find all '_context.next = <number>'; statements and note the 'case <number>:' they are referring to
		ExpressionStatement: function (path) {
			if (t.isAssignmentExpression(path.node.expression) &&
				path.node.expression.operator === '=' &&
				t.isMemberExpression(path.node.expression.left) &&
				t.isThisExpression(path.node.expression.left.object) &&
				t.isIdentifier(path.node.expression.left.property, { name: 'next' }) &&
				t.isNumericLiteral(path.node.expression.right)) {
				// Make a note directly on the node ..
				path.node.caseClause = this.caseClauses[path.node.expression.right.value];
			}
		}
	}

	const stage6 = {
		// Find our 'this.Present(something);' calls (we marked them so easy to find)
		// we create a new 'case ?:' clause for them
		CallExpression: function (path) {
			if (path.node.presentCall) {
				// This is one of our Present calls, need to create a new 'case :' clause for it
				let caseClause = path.findParent(p => t.isSwitchCase(p));

				// Extract body of the new case clause 
				let newCaseBody = caseClause.node.consequent.splice(path.parentPath.key - 1);     // Take the '_context.next = ?;' before it aswell

				// The <test> in 'case <test>:' value can be anything right now since we dont know it until all cases have been rendered and indexed later
				// we hook up the '_context.next = <test>' assignment directly to its 'case <test>:' reference instead, 
				let newCaseTest = -1;
				let contextNextAssignment = t.expressionStatement(t.assignmentExpression('=', t.memberExpression(this.contextId, t.identifier('next')), t.numericLiteral(newCaseTest)));
				contextNextAssignment.caseClause = t.switchCase(t.numericLiteral(newCaseTest), newCaseBody);

				// Close off the previous case clause
				caseClause.node.consequent.push(
					contextNextAssignment,
					t.breakStatement());

				// Insert our new case
				let caseIndex = this.switchClause.cases.indexOf(caseClause.node);
				this.switchClause.cases.splice(caseIndex + 1, 0, contextNextAssignment.caseClause);
			}
		}
	}

	const stage7 = {
		// Update all marked '_context.next = ?'; assignments with the correct 'case <test>:' value
		ExpressionStatement: function (path) {
			if (path.node.caseClause) {
				// use clauses new test
				path.node.expression.right.value = path.node.caseClause.test.value;
			}
		}
	}

	return {
		visitor: {
			Program(path) {
				let context = {
					contextId: t.thisExpression(),
					caseClauses: {}
				};
				path.traverse(stage1, context);
				path.traverse(stage2, context);
				// Gen intermediate code, this is stage3 done by Babel
				path.traverse(regenerator_runtime_plugin(babel).visitor, {
					opts: {}
				});

				path.traverse(stage4, context);
				// Make sure the Lingo function is async ..
				context.lingoFunction.async = true;

				// find all 'case <test>:' clauses and index them on their current <test> value
				context.switchClause.cases.forEach(caseClause => {
					context.caseClauses[caseClause.test.value] = caseClause;
				});

				// find all '_context.next = <test>;' assignments
				path.traverse(stage5, context);

				// Insert the new cases, then we can re-index the cases and their '_context.next = ' references
				path.traverse(stage6, context);

				// re-index the case <test> values
				context.switchClause.cases.forEach((caseClause, index) => {
					caseClause.test.value = index;
				});

				// and update all '_context.next = <test>;' assignments 
				path.traverse(stage7, context);
			}
		}
	}
}