
import express = require('express');
require('express-async-errors');
const port = process.env.port || 80;
const virtualDirPath = process.env.virtualDirPath || '';
console.log(`Virtual path is ${virtualDirPath}`);

const app = express();

import { OfflineChecklistModel, transpileChecklist } from './transpile-checklist';

const api = express.Router();


// This endpoint expects a JSON Body, used by Restful client
/*api.post(`/lingo-js-transpile`, express.json(), async (req, res) => {
	let source = req.body.source || ``;
	let output = await lingoJsTranspile(source);
	output.source = source;
	res.send({
		Source: source,
		Output: output.code,
		SourceMap: JSON.stringify(output.map),
		Errors: output.errors
	});
});

// This endpoint expects a plain text Body, useful for debugging via Postman
api.post(`/lingo-js-transpile-text`, express.text(), async (req, res) => {
	let source = typeof req.body === 'string' ? req.body : ``;
	let output = await lingoJsTranspile(source);
	output.source = source;
	if (req.param('codeOnly')) {
		res.send(output.code);
	} else {
		res.send(output);
	}
});
*/

interface TranspileChecklistRequest {
	checklist: OfflineChecklistModel;
	otherChecklists: OfflineChecklistModel[];
}

api.post(`/transpile-checklist`, express.json(), async (req, res) => {
	const request = req.body as TranspileChecklistRequest;
	const result = await transpileChecklist(request.checklist, request.otherChecklists);
	res.send(result);
})

app.use(`${virtualDirPath}/api`, api);

app.use((err, req, res, next) => {
	if (err.status) {
		res.status(err.status);
		res.json({
			error: err.message
		});
	} else {
		res.status(503);
		res.json({
			error: err.message
		})
	}

	next(err);
});

app.listen(port, () => {
	console.log(`Listening on ${port}`);
})